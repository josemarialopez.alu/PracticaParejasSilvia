FROM httpd:2.4
COPY ./html /usr/local/apache2/htdocs
COPY ./css/ficheroCss.css /usr/local/apache2/htdocs/css
EXPOSE 80

